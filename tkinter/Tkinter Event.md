- Component = Widget 
### Event = widget.bind(event,handler)
- `event is written like,Example : button.bind(<ButtonPress-1>,command)`
1. **1,2,3,Button-1,Button-2,Button-3,ButtonPress-1,ButtonPress-2,ButtonPress-3** `use to press mouse button`
2. **B1-Motion,B2-Motion,B3-Motion** `use to move mouse cursor`
3. **ButtonRelease-1,ButtonRelease-2,ButtonRelease-3** `use to release mouse button`
4. **Double-Button-1,Double-Button-2,Double-Button-3** `use to accept double click the mouse button` **Triple-Button-1,Triple-Button-2,Triple-Button-3** `use to accept double click the mouse button`
5. **Enter** `use to check the cursor is enter into to the gui component`
6. **Leave** `use to check the cursor is leave into to the gui component`
7. **FocusIn** `use to focus the keyboard to child component`
8. **FocusOut** `use to focus the keyboard of another component`
9. **Return** `use to get value or return key value of 102 in keyboard while press Enter button`
10. **a** `use for <space> and <line> event`
11. **Key** `use to get key value,while press the keyboard`
12. **Shift-Up,Control-Up,Alt-Up** `use to perform action like this Ctrl+V,Ctrl+C` 
13. **Configure** `use to configure or change a value in gui component`

### Event Attributes

1. x,y `to get mouse pointer of current position of tkinter`
2. x_root,y_root `to get mouse pointer of display position`
3. keycode `to get keyboard key character code`
4. widget `to get widget of the tkinter and even change that value`
5. keysum `to get key symbols`
6. char `to get key character`
7. num `to get mouse button number`
8. width,height `to get width and height of widget`
9. type `to get type of event`
