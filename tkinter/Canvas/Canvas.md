# Canvas

### Canvas Class Syntax

>  **w = Canvas(master,option = value, ....)**

### Canvas Class Parameters

- master  = To represents the Parent Window
- options = Attributes of Widget

| S.No | Option | Description | Default Value |
| ---- | ------ | ----------- | ------------- |
| 1 | background | To set background color | |
| 2 | bg         | To set background color | |
| 3 | borderwidth | To set border width of Canvas | 0 |
| 4 | bd | To set border width of Canvas | 0 |
| 5 | closeenough | To set closeEnough the canvas | 1 |
| 6 | confine     | Canvas cannot be scrolled outside of the scroll region | 1 |
| 7 | cursor | To change the cursor image | |
| 8 | height | To set the height of the Canvas | |
| 9 | highlightbackground | To set Color for border when the Canvas not have focus | |
| 10 | highlightcolor | To set Color for border when the Canvas have focus | |
| 11 | highligththickness | To set the width of the highlight border | 2 |
| 12 | insertbackground | To set the color to use for the text insertion cursor | |
| 13 | insertborderwidth | To set the width of the insertion cursor border, If insertborderwidth != 0,then the cursor use RAISED for border | |
| 14 | insertofftime | Together with insertontime,To set blinking of Cursor,value in millisecond | |
| 15 | insertontime | Together with insertofftime,To set blinking of Cursor,value in millisecond | |
| 16 | insertwidth | To set width of insertion cursor | 2 |
| 17 | offset | To set offset | "0,0" |
| 18 | relief | To set Border style "FLAT,RAISED,SUNKEN,GROOVE,RIDGE" | FLAT |
| 19 | scrollregion | To scroll the canvas | |
| 20 | selectbackground | To set selection background color | |
| 21 | selectborderwidth | To set selection border width | |
| 22 | selectforeground | To set text color | |
| 23 | state | To set the canvas start,"NORMAL,DISABLED,HIDDEN" | NORMAL |
| 24 | takefocus | Indicates that the user can use the Tab key to move to this widget.<br>takefocus = "",canvas accept focus only if it hava keyboard binding | "" |
| 25 | width | To set canvas width | |
| 26 | xscrollcommand | To set to scroll the canvas horizontally | |
| 27 | xscrollincreament | To set increament value to scroll horizontal | 0 | 
| 28 | yscrollcommand | To set to scroll the canvas vertically | |
| 29 | yscrollincreament | To set increment value to scroll vertical | 0 |



##### Canvas Item

1. arc (arc,chord,pieslice)
2. bitmap
3. image
4. line
5. oval
6. polygon
7. rectangle
8. text
9. window

##### Canvas Coordinate System

1. canvas.config(scrollregion = canvas.bbox(ALL))
2. canvas.canvasx(event.x)
3. canvas.canvasy(event.y)
4. canvas.find_closest(event.x,event.y)

##### Canvas Item Specifiers : Handles and Tags

1. item handles (integers) `integer values is assigned for identify specific item on canvas`
2. tags `Symbolic names attached to items` 
3. ALL `matches all items on the canvas`
4. CURRENT `matches all items under mouse pointer`

##### Printing 

1. It support Postscript printers


### Canvas Method :

#### create_arc

#### create_arc Syntax :
 
>  **canvas.create_arc(bbox,options)**

#### create_arc Parameters :

- bbox = Bounding box for the full arc
- **option = Option to create Arc

| S.No | Options | Description | Default value |
| --- | --- | --- | --- |
| 1 | activedash | Dash is use when the mouse pointer is moved over the item | |
| 2 | activefill | Fill color to use when the mouse pointer is moved over the item| |
| 3 | activeoutline | Outline is use when the mouse pointer is moved over the item | |
| 4 | activeoutlinestipple | Outlinestipple is use when the mouse pointer is moved over the item | |
| 5 | activestipple | Stipple is use when the mouse pointer is moved over the item | |
| 6 | activewidth | Width is use when the mouse pointer is moved over the item | 0 |
| 7 | dash| Outline dash pattern | |
| 8 | dashoffset | To set dashoffsets | 0 |
| 9 | disableddash | To disable the dash | |
| 10 | disabledfill | Fill color is use when the item is disabled | |
| 11 | disabledoutline | Outline is use when the item is disabled | |
| 12 | disabledoutlinestipple | Outlinestipple is use when the item is disabled | |
| 13 | disabledstipple | Stipple is use when the item is disabled | |
| 14 | disabledwidth | Width is use when the item is disabeld | 0 |
| 15 | extent | The size, relative to the start angle | 90.0 |
| 16 | fill | Fill color,An empty string means transparent | |
| 17 | offset | To set offset | "0,0" |
| 18 | outline | Outline color | "black" |
| 19 | outlineoffset | To set Outline offset | "0,0" |
| 20 | outlinestipple | outlinestipple pattern | |
| 21 | start | start angle | 0.0 |
| 22 | state | Item state,"NORMAL,DISABLED,HIDDEN" | NORMAL |
| 23 | stipple | Stripple pattern | |
| 24 | style | one of PIESLICE,CHORD,ARC | PIESLICE |
] 25 | tags | A tag to attach to this items, or a tuples containing multiple tags |  |
| 26 | width | to set Width | 1.0 |
  
#### create_arc Return :
   
1. Item Id


#### create_bitmap

#### create_bitmap Syntax :

> **canvas.create_bitmap(position,options)**

#### create_bitmap Parameters :

- position = Bitmap position,given as two coordinates
- options  = option of create_bitmap

| S.No | Options | Description |
| --- | --- | --- |
| 1 | activebackground | It use to change background color when cursor is on bitmap | |
| 2 | activebitmap | It use to change bitmap when cursor is on bitmap | |                    
| 3 | activeforeground | It use to change text color when cursor is on bitmap | |
| 4 | anchor | It use to set relative position | CENTER |
| 5 | background | To background color | |
| 6 | bitmap | To set BitmapImage | |
| 7 | disabledbackground | To disable the background color | |
| 8 | disabledbitmap | To disable the bitmap Image | |
| 9 | disabledforeground | To disable the text color | |
| 10 | foreground | To set Text color | |
| 11 | state | To set Item state."NORMAL,DISABLED,HIDDEN" | NORMAL |
| 12 | tags | A tag to attach to this item,or a tuple containing multiple tags | |

#### create_bitmap Return :

1. Item id



