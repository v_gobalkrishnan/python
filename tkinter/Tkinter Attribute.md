### Important Attribute 

1. Dimensions
2. Colors
3. Fonts
4. Anchors
5. Relief styles
6. Bitmaps
7. Cursors

### Tkinter Dimension

| S.NO | Character | Description |
| ---- | --------- | ----------- |
|   1  |     c     | Centimeters |
|   2  |     i     | Inches      |
|   3  |     m     | Millimeters |
|   4  |     p     | Printer points (1/72") |



### Tkinter Color

1. `#00ff00`
2. `#0000ff`
3. `#ff0000`
4. `#ffff00`
5. `#00ffff`
6. `#ff00ff`
7. `#000000`
8. `#ffffff`

### Tkinter Font
##### Eg: Font(family="Arial",size=23,weight="bold")

1. family
2. size
3. width

### Tkinter Anchors

1. N
2. S
3. W
4. E
5. NW
6. NE
7. SW
8. SE
9. CENTER

### Tkinter Relief

1. FLAT
2. RAISED
3. SUNKEN
4. GROOVE
5. RIDGE



### Tkinter Bitmaps

1. error
2. gray75
3. gray50
4. gray25
5. gray12
6. hourglass
7. info
8. questhead
9. question
10. warning


### Tkinter Cursors

1. arrow
2. circle
3. clock
4. cross
5. dotbox
6. exchange
7. fleur
8. heart
9. man
10. mouse
11. pirate
12. plus
13. shuttle
14. sizing
15. spider
16. spraycan
17. star
18. target
19. tcross
20. trek
21. watch

 

 

 
