# Button

### Syntax

> **w = Button (master,option=value,.....)**

### Parameters

- master  = parent window
- options = attribute for widget

| S.No | Option | Description | Default Value |
| ---- | ------ | ----------- | ------------- |
| 1 | activebackground | To change background color,when cursor enter the button | |
| 2 | activeforeground | To change foreground color,when cursor enter the button | |
| 3 | bd               | Border width | 2 |
| 4 | bg               | Background color | |
| 5 | command          | To perform method action when button is clicked | |
| 6 | fg               | foreground (text color) | |
| 7 | font             | to set text font | |
| 8 | height           | to set height of button | |
| 9 | highlightcolor   | if widget as focus,to set color of the focus hightlight | |
| 10 | image           | To display image to button | |
| 11 | justify         | To align Text "LEFT,RIGHT,CENTER" | CENTER |
| 12 | padx            | To padding left and right of text | |
| 13 | pady            | To padding top and bottom of text | |
| 14 | relief          | It specifiecs type of Border "FLAT,RAISED,SUNKEN,RAISED,GROOVE,RIDGE" | |
| 15 | state           | if state=DISABLED, is unresponsive.state=NORMAL, Button start to work | NORMAL |
| 16 | underline       | To underline the text in button.set underline=1 | -1 |
| 17 | width           | to set Width of Button | |
| 18 | wraplength      | if value in +ve,it fit into given length | |


### Method

| S.No | Method | Description |
| ---- | ------ | ----------- |
| 1 | flash() | Causes the button to flash several times between active and normal colors |
| 2 | invoke() |  Call's the button's callback <br> returns what the function return -not work when state=DISABLED |


